import React from "react";
import FadeInSection from "../FadeIn/FadeIn";
import {faChevronLeft, faChevronRight, faPlus} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import ScientificActivities from '../../data/ScientificActivities.json';

export default function Events() {
    return (
        <div className={"event-page"}>
            <FadeInSection key={2}>
                <div className={"title blue"}>Научные мероприятия</div>
                <div className={"blocks"}>
                    {ScientificActivities.map(({name, task, startdata, enddata}, index) => {
                        var image = "/images/events/" + parseInt(index + 1) +".svg"
                        return (
                            <div className={"block-row"}>
                                <div className={"img-place"}>
                                    <div className={"type"}>
                                        <div className={"type-text"}>Конкурс</div>
                                    </div>
                                    <img src={image} alt={""}/>
                                </div>
                                <div className={"text-place"}> 
                                    <div className={"text"}>{name}</div>
                                    <div className={"about"}>{task}</div>
                                    <div className={"date"}>
                                        <img src={"/images/icon/calendar.svg"} width={"20px"}
                                             height={"20px"}/> {startdata} - {enddata}
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </FadeInSection>
        </div>
    )
}