import { faArrowRight, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import {Bar} from 'react-chartjs-2';
import FadeInSection from "../FadeIn/FadeIn";
import { useState, useEffect, useRef } from 'react'

export default function MainPage({ data: serverData }) {
      const state = {
            labels: ['Психология', 'Право', 'Физика', 'Информационная инженерия'],
            datasets: [
                {
                    label: 'Count',
                    backgroundColor: '#4AACEB',
                    borderColor: 'rgba(60,143,196,0.4)',
                    borderWidth: 1,
                    data: [518, 407, 338, 272],
                    hoverBackgroundColor: '#1C53F4',
                    maxBarThickness: 120,
                }

            ]
        }
    const [data, setData] = useState(serverData)
    useEffect(() => {
        async function load() {
            const response = await fetch('api/v1/rsue/?search=ринх')
            const json = await response.json()
            setData(json)
        }

        if (!serverData) {
            load()
        }

    }, [])

    if (!data) {
        return (
            <p>Loading ...</p>
        )
    }
    return (
        <div className={"main-page"}>
            <FadeInSection key={1}>
                <div className={"hello-text"}>Привет, Admin RSUE!</div>

                <div className={"blocks"}>
                    <div className={"block-work"}>
                        <div className={"number"}>2</div>
                        <div className={"text"}>Статей написано</div>
                    </div>
                    {/* <div className={"block-work"}>
                        <div className={"number"}>2</div>
                        <div className={"text"}>Готовых статей</div>
                    </div> */}
                </div>

                <div className={"subtitle-button"}>Мои работы <div className={"add"}>
                    <div className={"add-img"}>
                        <FontAwesomeIcon icon={faPlus} />
                    </div>
                </div></div>


                <div className={"blocks"}>
                    {data.results.map((data, index) => {
                        var numImage = "/images/articles/"+ parseInt(index + 1) +".svg"
                        return (
                            <div className={"block"}>
                                <div className={"type"}>
                                    <div className={"type-text"}>{data.theme}</div>
                                </div>
                                <img src={numImage} alt={""} />
                                <div className={"text"}><a href={data.UrlFile}>{data.topic}</a></div>
                                <div className={"date"}>Автор: {data.authors}
                                </div>
                            </div>
                        )
                    })}

                    <div className={"arrow-next"}>
                        <FontAwesomeIcon icon={faArrowRight} />
                    </div>

                </div>

                <div className={"subtitle"}>Научные мероприятия</div>
                <div className={"blocks"}>

                    <div className={"block"}>
                        <div className={"type"}>
                            <div className={"type-text b"}>Docker</div>
                        </div>
                        <img src={"/images/events/1.svg"} alt={""} />
                        <div className={"text"}>Docker, каким бы я хотел его видеть</div>
                        <div className={"date"}><img src={"/images/icon/calendar.svg"} /> 28.11.2020
                    </div>
                    </div>

                    <div className={"block"}>
                        <div className={"type"}>
                            <div className={"type-text b"}>Java</div>
                        </div>
                        <img src={"/images/events/2.svg"} alt={""} />
                        <div className={"text"}>10 приложений для изучения Java на Android-устройствах</div>
                        <div className={"date"}><img src={"/images/icon/calendar.svg"} /> 28.11.2020
                    </div>
                    </div>

                    <div className={"block"}>
                        <div className={"type"}>
                            <div className={"type-text b"}>ML</div>
                        </div>
                        <img src={"/images/events/3.svg"} alt={""} />
                        <div className={"text"}>Нейросети: как искусственный интеллект помогает в бизнесе и жизни</div>
                        <div className={"date"}><img src={"/images/icon/calendar.svg"} /> 28.11.2020
                    </div>
                    </div>

                    <div className={"arrow-next"}>
                        <FontAwesomeIcon icon={faArrowRight} />
                    </div>
                </div>
                <div className={"subtitle"}>Приоритетные направления работ</div>
                <div className={"graf"}>
                    <Bar
                        data={state}
                        options={{
                            legend: {
                                display: false,
                                position: 'right'
                            }
                        }}
                    />
                </div>
            </FadeInSection>
        </div>
    )
}
MainPage.getInitialProps = async ({ req }) => {
    if (!req) {
        return { data: null }
    }
    const response = await fetch('api/v1/rsue/?search=ринх')
    const data = await response.json()

    return {
        data
    }
}