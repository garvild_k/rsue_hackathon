import React from "react";
import FadeInSection from "../FadeIn/FadeIn";
import { faChevronLeft, faChevronRight, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import { useState, useEffect, useRef } from 'react'
import ReactPaginate from 'react-paginate';
import { useRouter, } from "next/router";


export default function People() {
    const [data, setData] = useState([])
    const [query, setQuery] = useState("")
    const [apiParams, setApiParams] = useState({})
    const [pageCount, setPageCount] = useState(0)
    const selectedPage = useRef(0)
    let offset = 0


    useEffect(() => {
        const url = new URL('/api/v1/rsue/', window.location.origin)
        Object.keys(apiParams).forEach(key => url.searchParams.append(key, apiParams[key]))
        const timer = setTimeout(() => setDotsActive(true), 2000)
        fetch(url)
            .then(response => {
                if (response.status > 400) {
                    alert("Something went wrong!")
                }
                return response.json();
            })
            .then(data => {
                setData(data.results)
                setPageCount(data.count / 3);
                clearTimeout(timer)
            })
            .catch(error => alert(error));
    }, [apiParams]);

    const getFromApi = (e) => {
        e.preventDefault()
        const temp = {}
        if (query !== "") temp.search = query
        setApiParams(temp)
    }

    //"обработка кнопок пагинации (стрелочки)"
    //добавляет к запросу к апи параметр page и вызывает подключение к апи
    const handlePageClick = (e) => {
        selectedPage.current = e.selected
        const temp = {
            ...apiParams,
            page: selectedPage.current + 1
        }
        setApiParams(temp)
    }

    //сама пагинация
    const Paginate = () => {
        return (
                <ReactPaginate
                    forcePage={selectedPage.current}
                    previousLabel={<FontAwesomeIcon icon={faChevronLeft} className={"arrow"}/>}
                    nextLabel={<FontAwesomeIcon icon={faChevronRight} className={"arrow"}/>}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={1}
                    onPageChange={handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
        )
    }

    return (
        <div className={"teams-page"}>
            <FadeInSection key={1}>

                <form onSubmit={getFromApi} method="get">
                    <input name="q" placeholder="Поиск" type="search" onChange={e => setQuery(e.target.value)} />
                    <button type="submit"><FontAwesomeIcon icon={faSearch} /></button>
                </form>


                <div className={"title blue"}>Участники</div>

                <div className={"blocks"}>
                    {data.map((data) => {
                        return (
                            <div className={"block-row"}>
                                <div className={"img-place"}>
                                    <div className={"type"}>
                                        <div className={"type-text"}>{data.theme}</div>
                                    </div>
                                    <img src={"/images/person/4.svg"} alt={""} />
                                </div>


                                <div className={"text-place"}>
                                    <div className={"text"}>{data.authors}</div>
                                    <div className={"text-subtitle"}>
                                        <span className={"blue"}>Последняя статья: </span> <b>{data.topic}</b>
                                    </div>

                                    <div className={"blocks"}>
                                        <div className={"cope_text line-clamp"}>
                                        <p>{data.annotation}<br></br></p>
                                        
                                        </div>
                                        <a href={data.UrlFile}>Скачать</a>
                                    </div>
                                </div>
                            </div>)
                    })}


                <Paginate/>
                </div>

            </FadeInSection>
        </div>
    )
}