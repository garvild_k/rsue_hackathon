import React from "react";
import FadeInSection from "../FadeIn/FadeIn";
import { faChevronLeft, faChevronRight, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import { useState, useEffect, useRef } from 'react'
import ReactPaginate from 'react-paginate';
import { useRouter, } from "next/router";


export default function People({ data: serverData }) {
    const [data, setData] = useState(serverData)
    const [url, setUrl] = useState('api/v1/rsue/')
    const [query, setQuery] = useState()
    const router = useRouter()
    useEffect(() => {
        async function load() {
            // var url = 'api/v1/rsue/'
            const response = await fetch(url)
            const json = await response.json()
            setData(json)
            console.log("url " + query)
        }

        if (!serverData) {
            load()
        }

    }, [])

    if (!data) {
        return (<p>Loading ...</p>)
    }

    //формирование параметров запроса поиска
    const getFromApi = (e) => {
        e.preventDefault()
        const path = router.pathname
        const temp = {}
        if (query !== "") temp.search = query
        router.push({
            pathname: path,
            query: temp,
        })
        console.log("TEMP " + temp.search)
        setQuery(temp.search)

    }


    return (
        <div className={"teams-page"}>
            <FadeInSection key={1}>

                <form onSubmit={getFromApi} method="get">
                    <input name="q" placeholder="Поиск" type="search" onChange={e => setQuery(e.target.value)} />
                    <button type="submit"><FontAwesomeIcon icon={faSearch} /></button>
                </form>

                <div className="select">
                    <select>
                        <option>Участники</option>
                        <option>Команды</option>
                        <option>Все</option>
                    </select>
                    <div className="select__arrow" />
                </div>


                <div className={"title blue"}>Команды и участники</div>

                <div className={"blocks"}>
                    {data.results.map((data) => {
                        return (
                            <div className={"block-row"}>
                                <div className={"img-place"}>
                                    <div className={"type"}>
                                        <div className={"type-text"}>{data.theme}</div>
                                    </div>
                                    <img src={"/images/person/4.svg"} alt={""} />
                                </div>


                                <div className={"text-place"}>
                                    <div className={"text"}>{data.authors}</div>
                                    <div className={"text-subtitle"}>
                                        <span className={"blue"}>Последняя статья: </span> <b>{data.topic}</b>
                                    </div>

                                    <div className={"blocks"}>
                                        <div className={"block-work"}>
                                            <div className={"number"}>12</div>
                                            <div className={"text-t"}>Участий<br /> в конкурсах</div>
                                        </div>
                                        <div className={"block-work"}>
                                            <div className={"number"}>3</div>
                                            <div className={"text-t"}>Завершенных<br /> статей</div>
                                        </div>
                                        <div className={"block-work"}>
                                            <div className={"number"}>5</div>
                                            <div className={"text-t"}>Выигранных<br /> грантов</div>
                                        </div>
                                    </div>
                                </div>
                            </div>)
                    })}


                    <div className={"pagination"}>
                        <div className={"arrow"}>
                            <FontAwesomeIcon icon={faChevronLeft} />
                        </div>
                        <div className={"link active"}>
                        </div>
                        <div className={"link"}>
                            2
                        </div>
                        <div className={"link"}>
                            3
                        </div>
                        <div className={"link"}>
                            ...
                        </div>
                        <div className={"link"}>
                            50
                        </div>
                        <div className={"arrow active"}>
                            <FontAwesomeIcon icon={faChevronRight} />
                            { }
                        </div>
                    </div>
                </div>

            </FadeInSection>
        </div>
    )
}

People.getInitialProps = async ({ req, query }) => {
    if (!req) {
        return { data: null }
    }
    const response = await fetch('api/v1/rsue/')
    const data = await response.json()

    setResultsCount(data.count)
    setResults(1000)
    setPageCount(data.count / 3);

    return {
        data
    }
}