from django.urls import path, re_path
from . import views
from elasticSearch.views import RsueList

urlpatterns = [
    path('api/v1/rsue/', RsueList.as_view({'get': 'list'})),
    path('api/analytics/', views.analytics),
    # path('api/v1/point/', PointList.as_view()),
    # re_path(r'.*', views.index),
]
